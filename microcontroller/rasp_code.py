import time
import serial

ser = serial.Serial("/dev/ttyUSB0", 9600)  # Configura a serial e a velocidade de transmissao

while 1:
    resposta = ser.readline()  # Aguarda resposta
    # resposta.decode("utf-8")
    serial_arduino = str(resposta).split(",")
    ultrasonic_front = float(serial_arduino[1])
    ultrasonic_right = float(serial_arduino[2])
    ultrasonic_left = float(serial_arduino[3])
    ultrasonic_back = float(serial_arduino[4])

    infra_front = float(serial_arduino[5])
    infra_right = float(serial_arduino[6])
    infra_left = float(serial_arduino[7])
    infra_back = float(serial_arduino[8])

    stop_by_infrared = 0.0
    if infra_front == 1.0 or infra_right == 1.0 or infra_back == 1.0 or infra_left == 1.0:
        stop_by_infrared = 1.0
    else:
        stop_by_infrared = 0.0
    time.sleep(0.2)
