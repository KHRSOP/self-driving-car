import cv2
import pytesseract
import numpy as np


def stackImages(scale, imgArray):
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        hor_con = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver


def empty(a):
    pass


def getCountour(img, imgCountour):
    countors, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    for cnt in countors:
        area = cv2.contourArea(cnt)
        area_min = cv2.getTrackbarPos("Area", "Parameters")
        if area > area_min:
            cv2.drawContours(imgCountour, cnt, -1, (255, 0, 255), 7)
            peri = cv2.arcLength(cnt, True)
            approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
            print(len(approx))
            x, y, w, h = cv2.boundingRect(approx)
            cv2.rectangle(imgCountour, (x, y), (x + w, y + h), (0, 255, 0), 5)
            cv2.putText(imgCountour, "Pontos: " + str(len(approx)), (x + w + 20, y + 20),
                        cv2.FONT_HERSHEY_COMPLEX, .7, (0, 255, 0), 2)
            cv2.putText(imgCountour, "Area: " + str(int(area)), (x + w + 20, y + 45),
                        cv2.FONT_HERSHEY_COMPLEX, .7, (0, 255, 0), 2)


# parametros para trackbar
# cv2.namedWindow("Parameters")
# cv2.resizeWindow("Parameters", 640, 240)
# cv2.createTrackbar("Threshold1", "Parameters", 110, 255, empty)
# cv2.createTrackbar("Threshold2", "Parameters", 220, 255, empty)
# cv2.createTrackbar("Area", "Parameters", 2000, 30000, empty)

# referenciando arquivo executavel (instalado no PC)
# link de download: https://tesseract-ocr.github.io/tessdoc/Home.html
pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe'

# lendo imagem
img = cv2.imread('image.png')

# TODO: fazer funcionar detectar placa c/ x lados AND algarismo y


def moduloPrincipal():
    video = cv2.VideoCapture('http://192.168.0.15:8080/video')
    video.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
    video.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
    while True:
        success, frame = video.read()
        imgBlur = cv2.GaussianBlur(frame, (7, 7), 1)
        imgGray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # recuperando valores das trackbars e inserindo como parametro na função Canny()
        threshold1 = cv2.getTrackbarPos("Threshold1", "Parameters")
        threshold2 = cv2.getTrackbarPos("Threshold2", "Parameters")
        imgCanny = cv2.Canny(imgGray, threshold1, threshold2)

        # remover ruídos através de dilatação
        kernel = np.ones((5, 5))
        imgDilate = cv2.dilate(imgCanny, kernel, iterations=1)

        imgCountour = frame.copy()
        getCountour(imgDilate, imgCountour)

        imgStack = stackImages(0.8, ([frame, imgBlur, imgGray],
                                     [imgCanny, imgDilate, imgCountour]))

        cv2.imshow("Result", imgStack)
        key = cv2.waitKey(1)
        if key == 27:
            break


# moduloPrincipal()

# pytesseract somente aceita RGB e opencv é BGR
# logo, vamos converter

# video para texto -- não funciona direito
# video = cv2.VideoCapture('http://192.168.0.15:8080/video')
# video.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
# video.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)
# while True:
#     success, frame = video.read()
#     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#
#     output_text = pytesseract.image_to_string(frame)
#     print(output_text)
#
#     cv2.imshow("Result", frame)
#     key = cv2.waitKey(1)
#     if key == 27:
#         break

# imagem para texto
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
output_text = pytesseract.image_to_string(img)
print(output_text)

# exibindo imagem
cv2.imshow('result', img)
cv2.waitKey(0)
