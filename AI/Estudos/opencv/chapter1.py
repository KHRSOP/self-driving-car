# https://github.com/murtazahassan/Learn-OpenCV-in-3-hours
import cv2

# usando imagens
# img = cv2.imread("resources/lena.png")
# cv2.imshow("Output", img)
# cv2.waitKey(0)

# usando videos
# cap = cv2.VideoCapture("resources/testVideo.mp4")
#
# while True:
#     success, img = cap.read()
#     # success é true ou false
#
#     cv2.imshow("Video", img)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# usando webcams
cap = cv2.VideoCapture(0)  # id da câmera
# cap = cv2.VideoCapture('http://192.168.0.15:8080/video')  # id da câmera
cap.set(3, 640)  # definindo largura
cap.set(4, 480)  # definindo altura
cap.set(10, 100)  # ajustando o brilho

while True:
    success, frame = cap.read()
    # success é true ou false

    cv2.imshow("Video", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):  # pressione q para sair da camera
        break
