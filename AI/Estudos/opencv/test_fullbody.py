import cv2
import numpy as np
import csv

##
width = []
height = []
w_h = []
w_h_np = []
##

bodyCascade = cv2.CascadeClassifier("resources/haarcascade_fullbody.xml")
cap = cv2.VideoCapture("resources/video.mp4")
# cap = cv2.VideoCapture('http://192.168.0.15:8080/video')

while True:
    success, frames = cap.read()
    # success é true ou false

    gray = cv2.cvtColor(frames, cv2.COLOR_BGR2GRAY)

    full_body = bodyCascade.detectMultiScale(gray, 1.3, 2)
    for (x, y, w, h) in full_body:
        cv2.rectangle(frames, (x, y), (x + w, y + h), (255, 0, 0), 2)
        width.append(w)
        height.append(h)

    w_h = [width, height]
    w_h_np = np.asarray(w_h)
    np.savetxt("data.csv", w_h_np, delimiter=",")

    cv2.imshow("Video", frames)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
