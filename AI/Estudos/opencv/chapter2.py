import cv2
import numpy as np

img = cv2.imread("resources/lena.png")

# definindo tamanho da matriz e também todos os valores dela serão 1, e tbm define o tipo dos objetos
kernel = np.ones((5, 5), np.uint8)

# convertendo para gray scale
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# blur
imgBlur = cv2.GaussianBlur(imgGray, (7, 7), 0)

# edge detector (detecção de borda)
imgCanny = cv2.Canny(img, 150, 200)

# increase the tickness of the edge (aumentando a largura da borda)
# segundo parametro define o tamanho da 'matriz', por isso utiliza-se numpy
# terceiro parametro define quantas interações queremos que o kernel mova (quanto de tickness precisamos)
imgDialation = cv2.dilate(imgCanny, kernel, iterations=1)

# oposto de dialation - fazer ficar mais fino
imgEroded = cv2.erode(imgDialation, kernel, iterations=1)

cv2.imshow("Gray image", imgGray)
cv2.imshow("Blur image", imgBlur)
cv2.imshow("Canny image", imgCanny)
cv2.imshow("Dialation image", imgDialation)
cv2.imshow("Eroded image", imgEroded)
cv2.waitKey(0)
