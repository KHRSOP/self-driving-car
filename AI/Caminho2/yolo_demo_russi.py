import cv2
import numpy as np


def findObjects(outputs, frame):
    height, width, channels = frame.shape
    bbox = []  # x, y, width and height
    class_ids = []
    confs = []

    for output in outputs:
        # XXX, 85
        for detection in output:
            # remove the first five elements
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            # good detection if is higher than 50%
            if confidence > conf_threshold:
                # save the width, height, x and y
                w, h = int(detection[2] * width), int(detection[3] * height)  # getting the pixels values instead %

                # that is the center point, so we need to subtract the wt or ht divide by 2
                x, y = int((detection[0] * width) - (w / 2)), int((detection[1] * height) - (h / 2))

                # box configuration
                bbox.append([x, y, w, h])
                class_ids.append(class_id)
                confs.append(float(confidence))

    # quantity of objects detected in the image
    # print(len(bbox))

    # removing boxes that are finding the same thing
    indices = cv2.dnn.NMSBoxes(
        bboxes=bbox,
        scores=confs,
        score_threshold=conf_threshold,
        nms_threshold=nms_threshold
    )  # which bbox keep using

    # drawing boxes
    # print(indices)
    for i in indices:
        # removing extra [] --> [[0]]
        i = i[0]
        box = bbox[i]
        x, y, w, h = box[0], box[1], box[2], box[3]
        # draw
        cv2.rectangle(
            img=frame,
            pt1=(x, y),
            pt2=(x + w, y + h),
            color=(255, 0, 255),
            thickness=2
        )
        cv2.putText(
            img=frame,
            text=f'{class_names[class_ids[i]].upper()} {round(confs[i] * 100, 2)}%',
            org=(x, y - 10),
            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            fontScale=0.6,
            color=(255, 0, 255),
            thickness=2
        )

    print(serialOutput(len(bbox)))


def serialOutput(status):
    # 0 --> nothing detected
    # 1 --> 1 or more object detected
    if status == 0:
        return 0
    elif status >= 1:
        return 1


# CONSTANTS
# getting the video from the camera
video = cv2.VideoCapture(0)
# video = cv2.VideoCapture('http://192.168.0.15:8080/video')

# width, height and target
whT = 320  # size compatible with yolo model
conf_threshold = 0.5

# the lower the value, the more aggressive it will be
# you will have a smaller number of boxes
# if finding a lot of boxes, just reduce the value
nms_threshold = 0.2

# LOAD CLASSIFICATION LABELS - Coco Names
classes_file = 'coco.names'
class_names = []
with open(classes_file, 'rt') as f:
    class_names = f.read().rstrip('\n').split('\n')
print(class_names)

# Load network model configuration and weights
# Download the weights and cfgs here --> https://pjreddie.com/darknet/yolo/
model_configuration = 'yolov3-320.cfg'
model_weights = 'yolov3-320.weights'

# Creating network
network = cv2.dnn.readNetFromDarknet(cfgFile=model_configuration, darknetModel=model_weights)
network.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)  # using OpenCV as backend
network.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

while True:
    success, frame = video.read()

    # we cant use the image frame directly in the network, we need to convert
    # convert the frame to blob --> format that the network understand
    blob = cv2.dnn.blobFromImage(frame, 1 / 255, (whT, whT), [0, 0, 0], 1, crop=False)
    network.setInput(blob)

    # according to the convolution network model that we are using, we will have 3 different output
    # predict one, two and three
    # get the names of the layers
    layers_names = network.getLayerNames()

    # index of the outputs
    # print(network.getUnconnectedOutLayers())  # 200 - 1 = 199 --> actual value

    # intersect the index with the name of the output
    output_names = [layers_names[i[0] - 1] for i in network.getUnconnectedOutLayers()]

    # send this image to the network output
    outputs = network.forward(output_names)
    # print(outputs[0].shape)  # (300 rows, 85 cols) --> 300 = number of boxes;
    # print(outputs[1].shape)  # (1200 rows, 85 cols) --> 1200 = number of boxes;
    # print(outputs[2].shape)  # (4800 rows, 85 cols) --> 4800 = number of boxes;
    # explanation https://www.youtube.com/watch?v=xK4li3jinSw&t=240s
    # print(outputs[0][0])

    # extraction of the box and probability in output layer
    findObjects(outputs, frame)

    cv2.imshow("Camera", frame)

    key = cv2.waitKey(1)
    if key == 27:
        break
